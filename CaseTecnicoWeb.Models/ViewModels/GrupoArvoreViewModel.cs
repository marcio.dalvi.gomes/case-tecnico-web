﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CaseTecnicoWeb.Models.ViewModels
{
    public class GrupoArvoreViewModel
    {
        [DisplayName("Código")]
        public int GrupoArvoreId { get; set; }
        [DisplayName("Nome")]
        public string Nome { get; set; }
        [DisplayName("Descrição")]
        public string Descricao { get; set; }

        [NotMapped]
        [DisplayName("Arvores - Selecione Várias")]
        public virtual int[] ArvoresId { get; set; }
    }
}
