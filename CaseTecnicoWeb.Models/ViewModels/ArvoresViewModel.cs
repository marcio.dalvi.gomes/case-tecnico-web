﻿using CaseTecnicoWeb.Models.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace CaseTecnicoWeb.Models.ViewModels
{
    public class ArvoresViewModel
    {
        [DisplayName("Código")]
        public int ArvoresId { get; set; }
        [DisplayName("Descrição")]
        public string Descricao { get; set; }
        [DisplayName("Idade")]
        public int Idade { get; set; }

        [DisplayName("Espécie")]
        public int EspeciesId { get; set; }
        public virtual EspecieViewModel Especies {get; set;}
    }
}
