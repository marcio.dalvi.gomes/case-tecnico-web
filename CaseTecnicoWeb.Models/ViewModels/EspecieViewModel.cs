﻿using CaseTecnicoWeb.Models.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace CaseTecnicoWeb.Models.ViewModels
{
    public class EspecieViewModel
    {
        [DisplayName("Código")]
        public int EspeciesId { get; set; }
        [DisplayName("Espécie")]
        public string Descricao { get; set; }
    }
}
