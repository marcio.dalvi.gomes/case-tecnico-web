﻿using CaseTecnicoWeb.Models.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace CaseTecnicoWeb.Models.ViewModels
{
    public class ColheitaViewModel
    {
        [DisplayName("Código")]
        public int ColheitaId { get; set; }
        [DisplayName("Informações")]
        public string Informacoes { get; set; }
        [DisplayName("Data da colheita")]
        public DateTime DataDaColheita { get; set; }
        [DisplayName("Peso Bruto")]
        public float PesoBruto { get; set; }
        [DisplayName("Arvore")]
        public int ArvoresId { get; set; }
        public virtual ArvoresViewModel Arvores { get; set;}
    }
}
