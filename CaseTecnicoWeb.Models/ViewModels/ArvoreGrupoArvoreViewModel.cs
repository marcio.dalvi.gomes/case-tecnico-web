﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CaseTecnicoWeb.Models.ViewModels
{
    public class ArvoreGrupoArvoreViewModel
    {
        public int ArvoreGrupoArvoreId { get; set; }
        public int ArvoresId { get; set; }
        public int GrupoArvoreId { get; set; }
        public virtual ArvoresViewModel Arvores { get; set; }
        public virtual GrupoArvoreViewModel GrupoArvore { get; set; }
    }
}
