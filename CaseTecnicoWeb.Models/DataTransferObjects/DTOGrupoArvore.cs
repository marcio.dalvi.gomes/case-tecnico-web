﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseTecnicoWeb.Models.DataTransferObjects
{
    public class DTOGrupoArvore
    {
        public int GrupoArvoreId { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
    }
}
