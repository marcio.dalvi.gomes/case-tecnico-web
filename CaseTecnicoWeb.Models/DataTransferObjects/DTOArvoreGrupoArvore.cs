﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseTecnicoWeb.Models.DataTransferObjects
{
    public class DTOArvoreGrupoArvore
    {
        public int ArvoreGrupoArvoreId { get; set; }
        public int ArvoresId { get; set; }
        public int GrupoArvoreId { get; set; }
        public virtual DTOArvore Arvores { get; set; }
        public virtual DTOGrupoArvore GrupoArvore { get; set; }
    }
}
