﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseTecnicoWeb.Models.DataTransferObjects
{
    public class DTOColheita
    {
        public int ColheitaId { get; set; }
        public string Informacoes { get; set; }
        public DateTime DataDaColheita { get; set; }
        public float PesoBruto { get; set; }
        public int ArvoresId { get; set; }
        public virtual DTOArvore Arvores { get; set; }
    }
}
