﻿using CaseTecnicoWeb.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseTecnicoWeb.Models.DataTransferObjects
{
    public class DTOArvore
    {
        public int ArvoresId { get; set; }
        public string Descricao { get; set; }
        public int Idade { get; set; }
        public int EspeciesId { get; set; }
        public virtual DTOEspecie Especies { get; set; }

    }
}
