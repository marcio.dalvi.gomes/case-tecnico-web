﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseTecnicoWeb.Models.DataTransferObjects
{
    public class DTOEspecie
    {
        public int EspeciesId { get; set; }
        public string Descricao { get; set; }
    }
}
