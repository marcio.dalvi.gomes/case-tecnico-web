﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CaseTecnicoWeb.Service.Constants
{
    public class Constantes
    {
        public const string ARVORE_SERVICE = "https://casetecnico-api.azurewebsites.net/Arvores";
        public const string GRUPO_ARVORE_SERVICE = "https://casetecnico-api.azurewebsites.net/GrupoArvore";
        public const string ARVORE_GRUPO_ARVORE_SERVICE = "https://casetecnico-api.azurewebsites.net/ArvoreGrupoArvore";
        public const string COLHEITA_SERVICE = "https://casetecnico-api.azurewebsites.net/Colheita";
        public const string ESPECIES_SERVICE = "https://casetecnico-api.azurewebsites.net/Especies";
    }
}
