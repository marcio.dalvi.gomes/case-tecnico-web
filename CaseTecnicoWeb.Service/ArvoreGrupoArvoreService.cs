﻿using CaseTecnicoWeb.Models.DataTransferObjects;
using CaseTecnicoWeb.Models.ViewModels;
using CaseTecnicoWeb.Service.Spec;
using System;
using System.Collections.Generic;
using System.Text;
using CaseTecnicoWeb.Service.Constants;

namespace CaseTecnicoWeb.Service
{
    public class ArvoreGrupoArvoreService : IArvoreGrupoArvoreService
    {
        public bool Delete(int id)
        {
            var result = BaseRequests.Delete<bool>(Constantes.ARVORE_GRUPO_ARVORE_SERVICE, id);
            return result;
        }

        public bool DeleteMultiples(int[] ids)
        {
            var result = BaseRequests.DeleteMultiples<bool>(Constantes.ARVORE_GRUPO_ARVORE_SERVICE, ids);
            return result;
        }

        public DTOArvoreGrupoArvore Edit(int id, DTOArvoreGrupoArvore arvoreGrupoArvore)
        {
            var result = BaseRequests.Edit<DTOArvoreGrupoArvore>(Constantes.ARVORE_GRUPO_ARVORE_SERVICE, id,arvoreGrupoArvore);
            return result;
        }

        public List<DTOArvoreGrupoArvore> GetAll()
        {
            var result = BaseRequests.Get<List<DTOArvoreGrupoArvore>>(Constantes.ARVORE_GRUPO_ARVORE_SERVICE);
            return result;
        }

        public List<DTOArvoreGrupoArvore> GetByGrupoArvore(int id)
        {
            var result = BaseRequests.GetPersonalizado<List<DTOArvoreGrupoArvore>>(Constantes.ARVORE_GRUPO_ARVORE_SERVICE,"GrupoArvore",id);
            return result;
        }

        public DTOArvoreGrupoArvore GetById(int id)
        {
            var result = BaseRequests.GetById<DTOArvoreGrupoArvore>(Constantes.ARVORE_GRUPO_ARVORE_SERVICE,id);
            return result;
        }

        public bool Insert(DTOArvoreGrupoArvore arvoreGrupoArvore)
        {
            var result = BaseRequests.Create<bool>(Constantes.ARVORE_GRUPO_ARVORE_SERVICE,arvoreGrupoArvore);
            return result;
        }

        public bool InsertMultiples(List<DTOArvoreGrupoArvore> arvoreGrupoArvore)
        {
            var result = BaseRequests.CreatePersonalizado<bool>(Constantes.ARVORE_GRUPO_ARVORE_SERVICE, "Multiplos", arvoreGrupoArvore);
            return result;
        }
        public bool UpdateMultiples(List<DTOArvoreGrupoArvore> arvoreGrupoArvore)
        {
            var result = BaseRequests.EditPersonalizado<bool>(Constantes.ARVORE_GRUPO_ARVORE_SERVICE, "Multiplos", arvoreGrupoArvore);
            return result;
        }
    }
}
