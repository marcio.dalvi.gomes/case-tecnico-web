﻿using CaseTecnicoWeb.Models.DataTransferObjects;
using CaseTecnicoWeb.Models.ViewModels;
using CaseTecnicoWeb.Service.Spec;
using System;
using System.Collections.Generic;
using System.Text;
using CaseTecnicoWeb.Service.Constants;

namespace CaseTecnicoWeb.Service
{
    public class ArvoresService : IArvoresService
    {
        public bool Delete(int id)
        {
            var result = BaseRequests.Delete<bool>(Constantes.ARVORE_SERVICE, id);
            return result;
        }

        public DTOArvore Edit(int id, DTOArvore arvores)
        {
            var result = BaseRequests.Edit<DTOArvore>(Constantes.ARVORE_SERVICE, id,arvores);
            return result;
        }

        public List<DTOArvore> GetAll()
        {
            var result = BaseRequests.Get<List<DTOArvore>>(Constantes.ARVORE_SERVICE);
            return result;
        }

        public DTOArvore GetById(int id)
        {
            var result = BaseRequests.GetById<DTOArvore>(Constantes.ARVORE_SERVICE,id);
            return result;
        }

        public bool Insert(DTOArvore arvores)
        {
            var result = BaseRequests.Create<bool>(Constantes.ARVORE_SERVICE,arvores);
            return result;
        }
    }
}
