﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CaseTecnicoWeb.Service
{
    public static class BaseRequests
    {
        public static T Get<T>(string URL)
        {
            var client = new RestClient(URL);
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            var resultado = JsonConvert.DeserializeObject<T>(response.Content);
            return resultado;
        }
        public static T GetById<T>(string URL, int id)
        {
            var client = new RestClient(URL + $"/{id}");
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            var resultado = JsonConvert.DeserializeObject<T>(response.Content);
            return resultado;
        }
        public static T GetPersonalizado<T>(string URL, string personalizacao, int id)
        {
            var client = new RestClient(URL + $"/{id}/{personalizacao}");
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            var resultado = JsonConvert.DeserializeObject<T>(response.Content);
            return resultado;
        }
        public static T Delete<T>(string URL, int id)
        {
            var client = new RestClient(URL + $"/{id}");
            var request = new RestRequest(Method.DELETE);
            IRestResponse response = client.Execute(request);
            var resultado = JsonConvert.DeserializeObject<T>(response.Content);
            return resultado;
        }
        public static T DeleteMultiples<T>(string URL, int[] ids)
        {
            var client = new RestClient(URL);
            var request = new RestRequest(Method.DELETE);
            request.AddJsonBody(ids);
            IRestResponse response = client.Execute(request);
            var resultado = JsonConvert.DeserializeObject<T>(response.Content);
            return resultado;
        }
        public static T Create<T>(string URL, object objeto)
        {
            var client = new RestClient(URL);
            var request = new RestRequest(Method.POST);
            request.AddJsonBody(objeto);
            IRestResponse response = client.Execute(request);
            var resultado = JsonConvert.DeserializeObject<T>(response.Content);
            return resultado;
        }

        public static T CreatePersonalizado<T>(string URL, string personalizacao, object objeto)
        {
            var client = new RestClient(URL + $"/{personalizacao}");
            var request = new RestRequest(Method.POST);
            request.AddJsonBody(objeto);
            IRestResponse response = client.Execute(request);
            var resultado = JsonConvert.DeserializeObject<T>(response.Content);
            return resultado;
        }
        public static T EditPersonalizado<T>(string URL, string personalizacao, object objeto)
        {
            var client = new RestClient(URL + $"/{personalizacao}");
            var request = new RestRequest(Method.PUT);
            request.AddJsonBody(objeto);
            IRestResponse response = client.Execute(request);
            var resultado = JsonConvert.DeserializeObject<T>(response.Content);
            return resultado;
        }
        public static T Edit<T>(string URL, int id, object objeto)
        {
            var client = new RestClient(URL + $"/{id}");
            var request = new RestRequest(Method.PUT);
            request.AddJsonBody(objeto);
            IRestResponse response = client.Execute(request);
            var resultado = JsonConvert.DeserializeObject<T>(response.Content);
            return resultado;
        }
    }
}
