﻿using CaseTecnicoWeb.Models.DataTransferObjects;
using CaseTecnicoWeb.Models.ViewModels;
using CaseTecnicoWeb.Service.Spec;
using System;
using System.Collections.Generic;
using System.Text;
using CaseTecnicoWeb.Service.Constants;

namespace CaseTecnicoWeb.Service
{
    public class ColheitaService : IColheitaService
    {
        public bool Delete(int id)
        {
            var result = BaseRequests.Delete<bool>(Constantes.COLHEITA_SERVICE, id);
            return result;
        }

        public DTOColheita Edit(int id, DTOColheita colheita)
        {
            var result = BaseRequests.Edit<DTOColheita>(Constantes.COLHEITA_SERVICE, id,colheita);
            return result;
        }

        public List<DTOColheita> GetAll()
        {
            var result = BaseRequests.Get<List<DTOColheita>>(Constantes.COLHEITA_SERVICE);
            return result;
        }

        public DTOColheita GetById(int id)
        {
            var result = BaseRequests.GetById<DTOColheita>(Constantes.COLHEITA_SERVICE,id);
            return result;
        }

        public bool Insert(DTOColheita colheita)
        {
            var result = BaseRequests.Create<bool>(Constantes.COLHEITA_SERVICE, colheita);
            return result;
        }
    }
}
