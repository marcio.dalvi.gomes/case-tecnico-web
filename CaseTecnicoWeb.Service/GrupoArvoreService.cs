﻿using CaseTecnicoWeb.Models.DataTransferObjects;
using CaseTecnicoWeb.Models.ViewModels;
using CaseTecnicoWeb.Service.Spec;
using System;
using System.Collections.Generic;
using System.Text;
using CaseTecnicoWeb.Service.Constants;

namespace CaseTecnicoWeb.Service
{
    public class GrupoArvoreService : IGrupoArvoreService
    {
        public bool Delete(int id)
        {
            var result = BaseRequests.Delete<bool>(Constantes.GRUPO_ARVORE_SERVICE, id);
            return result;
        }

        public DTOGrupoArvore Edit(int id, DTOGrupoArvore grupoArvore)
        {
            var result = BaseRequests.Edit<DTOGrupoArvore>(Constantes.GRUPO_ARVORE_SERVICE, id,grupoArvore);
            return result;
        }

        public List<DTOGrupoArvore> GetAll()
        {
            var result = BaseRequests.Get<List<DTOGrupoArvore>>(Constantes.GRUPO_ARVORE_SERVICE);
            return result;
        }

        public DTOGrupoArvore GetById(int id)
        {
            var result = BaseRequests.GetById<DTOGrupoArvore>(Constantes.GRUPO_ARVORE_SERVICE,id);
            return result;
        }

        public DTOGrupoArvore Insert(DTOGrupoArvore grupoArvore)
        {
            var result = BaseRequests.Create<DTOGrupoArvore>(Constantes.GRUPO_ARVORE_SERVICE, grupoArvore);
            return result;
        }
    }
}
