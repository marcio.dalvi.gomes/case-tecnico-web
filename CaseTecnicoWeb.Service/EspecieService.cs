﻿using CaseTecnicoWeb.Models.DataTransferObjects;
using CaseTecnicoWeb.Models.ViewModels;
using CaseTecnicoWeb.Service.Spec;
using System;
using System.Collections.Generic;
using System.Text;
using CaseTecnicoWeb.Service.Constants;

namespace CaseTecnicoWeb.Service
{
    public class EspecieService : IEspecieService
    {
        public bool Delete(int id)
        {
            var result = BaseRequests.Delete<bool>(Constantes.ESPECIES_SERVICE, id);
            return result;
        }

        public DTOEspecie Edit(int id, DTOEspecie especie)
        {
            var result = BaseRequests.Edit<DTOEspecie>(Constantes.ESPECIES_SERVICE, id,especie);
            return result;
        }

        public List<DTOEspecie> GetAll()
        {
            var result = BaseRequests.Get<List<DTOEspecie>>(Constantes.ESPECIES_SERVICE);
            return result;
        }

        public DTOEspecie GetById(int id)
        {
            var result = BaseRequests.GetById<DTOEspecie>(Constantes.ESPECIES_SERVICE,id);
            return result;
        }

        public bool Insert(DTOEspecie especie)
        {
            var result = BaseRequests.Create<bool>(Constantes.ESPECIES_SERVICE,especie);
            return result;
        }
    }
}
