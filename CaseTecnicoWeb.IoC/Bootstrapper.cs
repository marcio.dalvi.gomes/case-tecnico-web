﻿using AutoMapper;
using CaseTecnicoWeb.Models.DataTransferObjects;
using CaseTecnicoWeb.Models.ViewModels;
using CaseTecnicoWeb.Service;
using CaseTecnicoWeb.Service.Spec;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace CaseTecnicoWeb.IoC
{
    public static class Bootstrapper
    {
        private static readonly Assembly[] assemblies =
      {
            Assembly.GetEntryAssembly(),
        };

        public static IServiceCollection AddBootstrapperIoC(this IServiceCollection services)
        {

            services
                .AddScoped<IEspecieService, EspecieService>()
                .AddScoped<IColheitaService, ColheitaService>()
                .AddScoped<IGrupoArvoreService, GrupoArvoreService>()
                .AddScoped<IArvoreGrupoArvoreService, ArvoreGrupoArvoreService>()
                .AddScoped<IArvoresService, ArvoresService>();

            services
               .AddAutoMapper(Bootstrapper.assemblies, ServiceLifetime.Transient);

            return services;
        }
        public static IServiceCollection AddAutoMapperBoostrapper(this IServiceCollection services)
        {
            var config = new AutoMapper.MapperConfiguration(cfg =>
            {
                #region DTO TO VIEW MODEL

                cfg.CreateMap<DTOArvore, ArvoresViewModel>();
                cfg.CreateMap<DTOColheita, ColheitaViewModel>();
                cfg.CreateMap<DTOEspecie, EspecieViewModel>();
                cfg.CreateMap<DTOGrupoArvore, GrupoArvoreViewModel>();
                cfg.CreateMap<DTOArvoreGrupoArvore, ArvoreGrupoArvoreViewModel>();
                #endregion
                #region VIEW MODEL TO DTO

                cfg.CreateMap<ArvoresViewModel, DTOArvore>();
                cfg.CreateMap<ColheitaViewModel, DTOColheita>();
                cfg.CreateMap<EspecieViewModel, DTOEspecie>();
                cfg.CreateMap<GrupoArvoreViewModel, DTOGrupoArvore>();
                cfg.CreateMap<ArvoreGrupoArvoreViewModel, DTOArvoreGrupoArvore>();
                #endregion
            });
            IMapper mapper = config.CreateMapper();
            services.AddSingleton(mapper);
            return services;
        }
    }
}
