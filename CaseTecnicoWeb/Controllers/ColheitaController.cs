﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CaseTecnicoWeb.Models.DataTransferObjects;
using CaseTecnicoWeb.Models.ViewModels;
using CaseTecnicoWeb.Service.Spec;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CaseTecnicoWeb.Controllers
{
    public class ColheitaController : Controller
    {

        private readonly IColheitaService _colheitaService;
        private readonly IArvoresService _arvoresService;
        IMapper _mapper;

        public ColheitaController(IColheitaService colheitaService, IMapper mapper, IArvoresService arvoresService)
        {
            _colheitaService = colheitaService;
            _mapper = mapper;
            _arvoresService = arvoresService;
        }

        // GET: ColheitaController
        public ActionResult Index()
        {
            ViewBag.Arvores = _mapper.Map<List<ArvoresViewModel>>(_arvoresService.GetAll());
            var retorno = _mapper.Map<List<ColheitaViewModel>>(_colheitaService.GetAll());
            return View(retorno);
        }
        public ActionResult Filtrar(int arvore, DateTime de, DateTime ate)
        {
            ViewBag.Arvores = _mapper.Map<List<ArvoresViewModel>>(_arvoresService.GetAll());
            var retorno = _mapper.Map<List<ColheitaViewModel>>(_colheitaService.GetAll());
            if (arvore != 0)
            {
                retorno = retorno.Where(x => x.ArvoresId == arvore).ToList();
            }
            retorno = retorno.Where(x => x.DataDaColheita.Date >= de.Date && x.DataDaColheita.Date <= ate.Date).ToList();
            return View("Index", retorno);
        }
        // GET: ColheitaController/Details/5
        public ActionResult Details(int id)
        {
            var retorno = _mapper.Map<ColheitaViewModel>(_colheitaService.GetById(id));
            return View(retorno);
        }

        // GET: ColheitaController/Create
        public ActionResult Create()
        {
            ViewBag.Arvores = _mapper.Map<List<ArvoresViewModel>>(_arvoresService.GetAll());
            return View(new ColheitaViewModel() { });
        }

        // POST: ColheitaController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ColheitaViewModel model)
        {
            try
            {

                if (string.IsNullOrEmpty(model.Informacoes) || model.PesoBruto <= 0 || model.ArvoresId <= 0)
                {

                    ViewBag.Arvores = _mapper.Map<List<ArvoresViewModel>>(_arvoresService.GetAll());
                    var retornoModel = _mapper.Map<List<ColheitaViewModel>>(_colheitaService.GetAll());
                    ViewBag.Error = "Preencha os campos corretamente para pode realizar a requisição";
                    return View("Index",retornoModel);
                }
                var retorno = _colheitaService.Insert(_mapper.Map<DTOColheita>(model));
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ColheitaController/Edit/5
        public ActionResult Edit(int id)
        {
            ViewBag.Arvores = _mapper.Map<List<ArvoresViewModel>>(_arvoresService.GetAll());
            var retorno = _mapper.Map<ColheitaViewModel>(_colheitaService.GetById(id));
            return View(retorno);
        }

        // POST: ColheitaController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, ColheitaViewModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.Informacoes) || model.PesoBruto <= 0 || model.ArvoresId <= 0)
                {

                    ViewBag.Arvores = _mapper.Map<List<ArvoresViewModel>>(_arvoresService.GetAll());
                    ViewBag.Error = "Preencha os campos corretamente para pode realizar a requisição";
                    return View(model);
                }
                var retorno = _colheitaService.Edit(id, _mapper.Map<DTOColheita>(model));
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ColheitaController/Delete/5
        public ActionResult Delete(int id)
        {
            var retorno = _mapper.Map<ColheitaViewModel>(_colheitaService.GetById(id));
            return View(retorno);
        }

        // POST: ColheitaController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, ColheitaViewModel model)
        {
            try
            {
                var retorno = _colheitaService.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
