﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CaseTecnicoWeb.Models.DataTransferObjects;
using CaseTecnicoWeb.Models.ViewModels;
using CaseTecnicoWeb.Service.Spec;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CaseTecnicoWeb.Controllers
{
    public class EspecieController : Controller
    {
        private readonly IEspecieService _especieService;
        IMapper _mapper;

        public EspecieController( IMapper mapper, IEspecieService especieService)
        {
            _mapper = mapper;
            _especieService = especieService;
        }

        // GET: EspecieController
        public ActionResult Index()
        {
            var retorno = _mapper.Map<List<EspecieViewModel>>(_especieService.GetAll());
            return View(retorno);
        }

        // GET: EspecieController/Details/5
        public ActionResult Details(int id)
        {
            var retorno = _mapper.Map<EspecieViewModel>(_especieService.GetById(id));
            return View(retorno);
        }

        // GET: EspecieController/Create
        public ActionResult Create()
        {
            return View(new EspecieViewModel() { });
        }

        // POST: EspecieController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(EspecieViewModel model)
        {
            try
            {
                var retorno = _especieService.Insert(_mapper.Map<DTOEspecie>(model));
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        // GET: EspecieController/Edit/5
        public ActionResult Edit(int id)
        {
            var retorno = _mapper.Map<EspecieViewModel>(_especieService.GetById(id));
            return View(retorno);
        }

        // POST: EspecieController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, EspecieViewModel model)
        {
            try
            {
                var retorno = _especieService.Edit(id, _mapper.Map<DTOEspecie>(model));
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: EspecieController/Delete/5
        public ActionResult Delete(int id)
        {
            var retorno = _mapper.Map<EspecieViewModel>(_especieService.GetById(id));
            return View(retorno);
        }

        // POST: EspecieController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, EspecieViewModel model)
        {
            try
            {
                var retorno = _especieService.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
