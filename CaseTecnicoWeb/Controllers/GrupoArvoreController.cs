﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CaseTecnicoWeb.Models.DataTransferObjects;
using CaseTecnicoWeb.Models.ViewModels;
using CaseTecnicoWeb.Service;
using CaseTecnicoWeb.Service.Spec;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CaseTecnicoWeb.Controllers
{
    public class GrupoArvoreController : Controller
    {
        private readonly IGrupoArvoreService _grupoArvoreService;
        private readonly IArvoreGrupoArvoreService _arvoreGrupoArvoreService;
        private readonly IArvoresService _arvoresService;
        IMapper _mapper;

        public GrupoArvoreController(IMapper mapper, IGrupoArvoreService grupoArvoreService, IArvoresService arvoresService, IArvoreGrupoArvoreService arvoreGrupoArvoreService)
        {
            _mapper = mapper;
            _grupoArvoreService = grupoArvoreService;
            _arvoresService = arvoresService;
            _arvoreGrupoArvoreService = arvoreGrupoArvoreService;
        }
        // GET: GrupoArvoreController
        public ActionResult Index()
        {
            var retorno = _mapper.Map<List<GrupoArvoreViewModel>>(_grupoArvoreService.GetAll());
            return View(retorno);
        }

        // GET: GrupoArvoreController/Details/5
        public ActionResult Details(int id)
        {
            var retorno = _mapper.Map<GrupoArvoreViewModel>(_grupoArvoreService.GetById(id));
            var arvoresDoGrupo = _mapper.Map<List<ArvoreGrupoArvoreViewModel>>(_arvoreGrupoArvoreService.GetByGrupoArvore(id));
            var arvores = arvoresDoGrupo.Select(x => x.Arvores);

            ViewBag.Arvores = new MultiSelectList(arvores, "ArvoresId", "Descricao");
            return View(retorno);
        }

        // GET: GrupoArvoreController/Create
        public ActionResult Create()
        {
            var todasArvores = _mapper.Map<List<ArvoresViewModel>>(_arvoresService.GetAll());
            ViewBag.Arvores = new MultiSelectList(todasArvores, "ArvoresId", "Descricao");
            return View(new GrupoArvoreViewModel() { });
        }

        // POST: GrupoArvoreController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(GrupoArvoreViewModel model)
        {
            try
            {
                if (model.ArvoresId.Length <= 0)
                {
                    return RedirectToAction(nameof(Index));
                }
                var retorno = _grupoArvoreService.Insert(_mapper.Map<DTOGrupoArvore>(model));
                if (retorno == null)
                    return RedirectToAction(nameof(Index));

                var lista = new List<DTOArvoreGrupoArvore>();
                foreach (var item in model.ArvoresId)
                {
                    lista.Add(new DTOArvoreGrupoArvore() { ArvoresId = item, GrupoArvoreId = retorno.GrupoArvoreId });
                }
                _arvoreGrupoArvoreService.InsertMultiples(lista);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: GrupoArvoreController/Edit/5
        public ActionResult Edit(int id)
        {
            var retorno = _mapper.Map<GrupoArvoreViewModel>(_grupoArvoreService.GetById(id));
            var todasArvores = _mapper.Map<List<ArvoresViewModel>>(_arvoresService.GetAll());
            ViewBag.Arvores = new MultiSelectList(todasArvores, "ArvoresId", "Descricao");
            var arvoresDoGrupo = _mapper.Map<List<ArvoreGrupoArvoreViewModel>>(_arvoreGrupoArvoreService.GetByGrupoArvore(id));
            retorno.ArvoresId = arvoresDoGrupo.Select(x => x.ArvoresId).ToArray();
            return View(retorno);
        }

        // POST: GrupoArvoreController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, GrupoArvoreViewModel model)
        {
            try
            {

                var retorno = _grupoArvoreService.Edit(id, _mapper.Map<DTOGrupoArvore>(model));
                if (retorno == null)
                    return RedirectToAction(nameof(Index));

                #region CarregarDados
                var arvoresDoGrupo = _mapper.Map<List<ArvoreGrupoArvoreViewModel>>(_arvoreGrupoArvoreService.GetByGrupoArvore(id));
                var arvoresDoGrupoIds = arvoresDoGrupo.Select(x => x.ArvoresId);
                var novasArvoresIds = model.ArvoresId.ToList();
                #endregion
                var arvoresParaRemover = arvoresDoGrupo.Where(x => !novasArvoresIds.Contains(x.ArvoresId)).ToList();
                var arvoresParaAdicionar = new List<ArvoreGrupoArvoreViewModel>();
                #region ArvoresParaAdd
                var arvoresFiltradas = _mapper.Map<List<ArvoresViewModel>>(_arvoresService.GetAll());
                arvoresFiltradas = arvoresFiltradas.Where(x => novasArvoresIds.Contains(x.ArvoresId) && !arvoresDoGrupoIds.Contains(x.ArvoresId)).ToList();
                if (arvoresFiltradas.Count > 0)
                {
                    foreach (var item in arvoresFiltradas)
                    {
                        arvoresParaAdicionar.Add(new ArvoreGrupoArvoreViewModel()
                        {
                            ArvoresId = item.ArvoresId,
                            GrupoArvoreId = id
                        });
                    };
                }
                #endregion

                if (arvoresParaRemover.Count > 0)
                {
                    _arvoreGrupoArvoreService.DeleteMultiples(arvoresParaRemover.Select(x => x.ArvoreGrupoArvoreId).ToArray());
                }
                if (arvoresParaAdicionar.Count > 0)
                {
                    _arvoreGrupoArvoreService.InsertMultiples(_mapper.Map<List<DTOArvoreGrupoArvore>>(arvoresParaAdicionar));
                }


                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: GrupoArvoreController/Delete/5
        public ActionResult Delete(int id)
        {
            var retorno = _mapper.Map<GrupoArvoreViewModel>(_grupoArvoreService.GetById(id));
            var arvoresDoGrupo = _mapper.Map<List<ArvoreGrupoArvoreViewModel>>(_arvoreGrupoArvoreService.GetByGrupoArvore(id));
            var arvores = arvoresDoGrupo.Select(x => x.Arvores);

            ViewBag.Arvores = new MultiSelectList(arvores, "ArvoresId", "Descricao");
            return View(retorno);
        }

        // POST: GrupoArvoreController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, GrupoArvoreViewModel model)
        {
            try
            {
                var retorno = _grupoArvoreService.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
