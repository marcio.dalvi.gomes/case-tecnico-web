﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CaseTecnicoWeb.Models.DataTransferObjects;
using CaseTecnicoWeb.Models.ViewModels;
using CaseTecnicoWeb.Service.Spec;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CaseTecnicoWeb.Controllers
{
    public class ArvoreController : Controller
    {

        private readonly IArvoresService _arvoresService;
        private readonly IEspecieService _especieService;
        IMapper _mapper;

        public ArvoreController(IArvoresService arvoresService, IMapper mapper, IEspecieService especieService)
        {
            _arvoresService = arvoresService;
            _mapper = mapper;
            _especieService = especieService;
        }

        // GET: ArvoreController
        public ActionResult Index()
        {
            var retorno = _mapper.Map<List<ArvoresViewModel>>(_arvoresService.GetAll());
            return View(retorno);
        }

        // GET: ArvoreController/Details/5
        public ActionResult Details(int id)
        {
            var retorno = _mapper.Map<ArvoresViewModel>(_arvoresService.GetById(id));
            return View(retorno);
        }

        // GET: ArvoreController/Create
        public ActionResult Create()
        {
            ViewBag.Especies = _mapper.Map<List<EspecieViewModel>>(_especieService.GetAll());
            return View(new ArvoresViewModel() { });
        }

        // POST: ArvoreController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ArvoresViewModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.Descricao) || model.Idade <= 0)
                {
                    var retornoindex = _mapper.Map<List<ArvoresViewModel>>(_arvoresService.GetAll());
                    ViewBag.Error = "Preencha os campos corretamente para pode realizar a requisição";
                    return View("Index", retornoindex);
                }
                var retorno = _arvoresService.Insert(_mapper.Map<DTOArvore>(model));
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ArvoreController/Edit/5
        public ActionResult Edit(int id)
        {

            ViewBag.Especies = _mapper.Map<List<EspecieViewModel>>(_especieService.GetAll());
            var retorno = _mapper.Map<ArvoresViewModel>(_arvoresService.GetById(id));
            return View(retorno);
        }

        // POST: ArvoreController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, ArvoresViewModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.Descricao) || model.Idade <= 0)
                {
                    ViewBag.Especies = _mapper.Map<List<EspecieViewModel>>(_especieService.GetAll());
                    ViewBag.Error = "Preencha os campos corretamente para pode realizar a requisição";
                    return View(model);
                }
                var retorno = _arvoresService.Edit(id, _mapper.Map<DTOArvore>(model));
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ArvoreController/Delete/5
        public ActionResult Delete(int id)
        {
            var retorno = _mapper.Map<ArvoresViewModel>(_arvoresService.GetById(id));
            return View(retorno);
        }

        // POST: ArvoreController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, ArvoresViewModel model)
        {
            try
            {
                var retorno = _arvoresService.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
