﻿using CaseTecnicoWeb.Models.DataTransferObjects;
using CaseTecnicoWeb.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace CaseTecnicoWeb.Service.Spec
{
    public interface IEspecieService
    {

        List<DTOEspecie> GetAll();
        bool Insert(DTOEspecie especie);
        DTOEspecie GetById(int id);
        DTOEspecie Edit(int id, DTOEspecie especie);
        bool Delete(int id);
    }
}
