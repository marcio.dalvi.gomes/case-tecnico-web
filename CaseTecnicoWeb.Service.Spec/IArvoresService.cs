﻿using CaseTecnicoWeb.Models.DataTransferObjects;
using CaseTecnicoWeb.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace CaseTecnicoWeb.Service.Spec
{
    public interface IArvoresService
    {

        List<DTOArvore> GetAll();
        bool Insert(DTOArvore arvores);
        DTOArvore GetById(int id);
        DTOArvore Edit(int id, DTOArvore arvore);
        bool Delete(int id);
    }
}
