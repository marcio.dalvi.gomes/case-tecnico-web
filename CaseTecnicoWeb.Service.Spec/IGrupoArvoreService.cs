﻿using CaseTecnicoWeb.Models.DataTransferObjects;
using CaseTecnicoWeb.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace CaseTecnicoWeb.Service.Spec
{
    public interface IGrupoArvoreService
    {

        List<DTOGrupoArvore> GetAll();
        DTOGrupoArvore Insert(DTOGrupoArvore grupoArvore);
        DTOGrupoArvore GetById(int id);
        DTOGrupoArvore Edit(int id, DTOGrupoArvore grupoArvore);
        bool Delete(int id);
    }
}
