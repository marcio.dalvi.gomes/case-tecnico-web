﻿using CaseTecnicoWeb.Models.DataTransferObjects;
using CaseTecnicoWeb.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace CaseTecnicoWeb.Service.Spec
{
    public interface IArvoreGrupoArvoreService
    {

        List<DTOArvoreGrupoArvore> GetAll();
        bool Insert(DTOArvoreGrupoArvore arvoreGrupoArvore);
        bool InsertMultiples(List<DTOArvoreGrupoArvore> arvoreGrupoArvore);
        bool UpdateMultiples(List<DTOArvoreGrupoArvore> arvoreGrupoArvore);
        DTOArvoreGrupoArvore GetById(int id);
        DTOArvoreGrupoArvore Edit(int id, DTOArvoreGrupoArvore arvoreGrupoArvore);
        List<DTOArvoreGrupoArvore> GetByGrupoArvore(int id);
        bool Delete(int id);
        bool DeleteMultiples(int[] ids);
    }
}
