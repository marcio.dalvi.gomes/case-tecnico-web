﻿using CaseTecnicoWeb.Models.DataTransferObjects;
using CaseTecnicoWeb.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace CaseTecnicoWeb.Service.Spec
{
    public interface IColheitaService
    {

        List<DTOColheita> GetAll();
        bool Insert(DTOColheita colheita);
        DTOColheita GetById(int id);
        DTOColheita Edit(int id, DTOColheita colheita);
        bool Delete(int id);
    }
}
